import { NgModule } from '@angular/core';
import { AccordionComponent } from './accordion.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';
import { AccordionItemHeadComponent } from './accordion-item-head/accordion-item-head.component';
import { AccordionItemBodyComponent } from './accordion-item-body/accordion-item-body.component';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [
    AccordionComponent,
    AccordionItemComponent,
    AccordionItemHeadComponent,
    AccordionItemBodyComponent
  ],
  exports: [ AccordionComponent ]
})
export class AccordionModule { }
