import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionItemHeadComponent } from './accordion-item-head.component';

describe('AccordionItemHeadComponent', () => {
  let component: AccordionItemHeadComponent;
  let fixture: ComponentFixture<AccordionItemHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionItemHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionItemHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
