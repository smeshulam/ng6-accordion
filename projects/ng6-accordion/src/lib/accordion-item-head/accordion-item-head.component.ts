import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { AccordionItemComponent } from '../accordion-item/accordion-item.component';


@Component({
  selector: 'accordion-item-head',
  templateUrl: './accordion-item-head.component.html',
  styleUrls: ['./accordion-item-head.component.scss']
})
export class AccordionItemHeadComponent implements OnInit {
  @Output() onToggled: EventEmitter<any> = new EventEmitter<any>();
  private item: AccordionItemComponent;


  constructor(@Inject(forwardRef(() => AccordionItemComponent)) accordionItemComponent: AccordionItemComponent) {
    this.item = accordionItemComponent;
  }

  ngOnInit() {
  }

  toggleClick(event: Event) {
    this.item.onToggle(this.item.collapsed);
  }

}
