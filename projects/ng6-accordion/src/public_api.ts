/*
 * Public API Surface of accordion
 */

export * from './lib/accordion-item/accordion-item.component';
export * from './lib/accordion-item-body/accordion-item-body.component';
export * from './lib/accordion-item-head/accordion-item-head.component';
export * from './lib/accordion.component';
export * from './lib/accordion.module';
